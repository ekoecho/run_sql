import os
import re
import argparse
import logging
from collections import OrderedDict
import mysql.connector

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

# args - dir, user, host, dbname, pass
parser = argparse.ArgumentParser()
parser.add_argument("dir", help="location of the script files")
parser.add_argument("user", help="Database user")
parser.add_argument("host", help="Database host")
parser.add_argument("db", help="Database name")
parser.add_argument("password", help="Database password")
args = parser.parse_args()


def get_scripts(scripts_dir):
    """List files in the db_scripts directory and parse their names to get version numbers.
    Returns a dict of version numbers and file names"""
    db_scripts = {}
    for filename in os.listdir(scripts_dir):
        result = re.search(r'^\d+', filename)
        if result:
            number = int(result.group())
            db_scripts[number] = filename
        else:
            logger.info("Ignoring file %s as it does not start with a number.", filename)

    return(db_scripts)

def get_version():
    """Get the current version from the database"""
    cnx = mysql.connector.connect(user=args.user,
                                  password=args.password,
                                  host=args.host,
                                  database=args.db)
    try:
        cur = cnx.cursor()
        cur.execute("SELECT * from versionTable")
        current_version = cur.fetchone()[0]
        cnx.commit()
        cur.close()
        logger.info("Current version is %i", current_version)
        return current_version

    except Exception as e:
        print(e)


def get_pending_scripts(scripts, current_version):
    """Compare keys in scripts dict to current version and create a pending scripts dict from any that have a
    version greater than 'current_version'"""
    # dict comprehension to remove any items below current version
    pending_scripts = {k: v for k, v in scripts.items() if k > current_version}
    # use OrderedDict to avoid the need to sort more than once
    sorted_pending_scripts = OrderedDict(sorted(pending_scripts.items()))
    logger.info("pending scripts: %s", sorted_pending_scripts.items())
    return sorted_pending_scripts


def main():

    scripts = get_scripts(args.dir)
    version = get_version()
    try:

        cnx = mysql.connector.connect(user=args.user,
                                      password=args.password,
                                      host=args.host,
                                      database=args.db)
        pending_scripts = get_pending_scripts(scripts, version)

        for k, script in pending_scripts.iteritems():
            logger.info("Executing: %s", script)
            with open(os.path.join(args.dir, script)) as m:
                # Clean up input removing any trailing space or newlines
                queries = m.read().strip().replace("\n", "").split(";")
                queries.remove("")
                try:
                    cur = cnx.cursor()
                    cnx.start_transaction()
                    for query in queries:
                        cur.execute(query)

                    logger.info("Updating Version to: %s", k )
                    cur.execute("UPDATE versionTable set version="+str(k))
                    cnx.commit()
                    cur.close()
                except Exception as e:
                    print(e)
                    cnx.rollback()
    except Exception as e:
        print(e)
    finally:
        cnx.close()


if __name__ == "__main__":
    main()
