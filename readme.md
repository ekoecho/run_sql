### MySQL Script Execution Tool

This script will run any numbered SQL scripts found in the specified location. The scripts will
be executed in numerical order. If a filename does not begin with a number it will be ignored.

The current 'version' number is tracked within the database. On successful execution of the SQL statements containted
within a file the version number is increased to match the number of the file.

The SQL statements within each file are executed within a single transaction. If a statement fails
the transaction will be rolled back and the version number will not be increased.

There are a few sample SQL files in the db_scripts directory, these can be used to test the behaviour
of the script execution tool.


#### Usage
To execute:

```python run_sql.py <script_dir> <db_user> <db_host> <db_name> <db_password>```


#### Requirements


Tested on MySQL 8.0.11 and Python 2.7.12

The MySQL database **must** have a table named versionTable with a populated version number.

**mysql-connector-python** - Required for executing the mysql queries in the script.

A Pipfile has been included alongside the python script. If pipenv
is available on the target host then the dependencies can
be installed with a simple ```pipenv install```

Other python dependency managers can be used e.g. ```pip install mysql-connector-python``` 

#### Assumptions
- Assume that none of the SQL statements are selects
- Assume that an SQL file may contain multiple SQL statements
and those statements do not contain comments. The files are 
split using the ";" character and it is possible that this character
could be found in a comment. This could be improved by escaping/removing
comments before splitting the file.
- Assume that a valid SQL file starts with a number. Files that do not start with a number are ignored.

#### Improvements
- Automatic creation of the versionTable and version number if none exists
- Add more specific exception handling
- Allow for comments in the SQL files



